/**
 * Created by Max on 14-02-16.
 */
module.exports = function (req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.json({error:'Not authenticated'});
};
