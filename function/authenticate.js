/**
 * Created by Max on 14-02-16.
 */
module.exports = function (req, res, next) {
    if (req.user){
        if (req.isAuthenticated()) {
            if (req.user.enabled){
                return next();
            } else {
                res.redirect('/logout');
            }
        } else {
            res.redirect('/login');
        }
    } else {
        res.redirect('/login');
    }
};
