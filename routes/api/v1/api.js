var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../../../schemas/User');
var Entity = require('../../../schemas/Entity');
var passport = require('passport');
var isAuthenticated = require('../../../function/authenticateApi');
var users = require('./users');


router.use(isAuthenticated);
router.use('/users', users);

/* GET api page. */
router.get('/', function(req, res, next) {
    res.json({ message: 'Welcome to the coolest API on earth!' });
});



module.exports = router;
