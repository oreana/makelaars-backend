var express = require('express');
var router = express.Router();
var User = require('../../../schemas/User');
var Entity = require('../../../schemas/Entity');
var crypto = require('crypto');
var generatePassword = require('password-generator');
var nodemailer = require('nodemailer');
//var transporter = nodemailer.createTransport('smtp://max.vandelaar%40me.com:jcqKWqEfqwj2@smtp.mail.me.com');
var transporter = nodemailer.createTransport('smtps://maem.vandelaar%40gmail.com:DaRkEyEdEm0@smtp.gmail.com');

var mailOptions = {
    //todo: change settings to production
    from: '"Max van de Laar" <max.vandelaar@me.com>', // sender address
    to: 'max.vandelaar@me.com', // list of receivers notated as: 'email@te.nl, email2@te.nl'
    subject: '', // Subject line
    text: '', // plaintext body
    html: '' // html body
};

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.get('/count/:entity', function (req, res, next) {
    console.log(req.params.entity);
    User.find({"entity": req.params.entity, "profile.role": "Client"}, function (err, users) {
        res.json({count: users.length});

    });
});

router.post('/new/employee', function (req, res, next) {
   //TODO: store all data from the form

    var password = generatePassword();

    User.register(new User(
            {
                username: req.body.email,
                email: req.body.email,
                entity: req.body.entity,
                profile: {
                    role: req.body.role,
                    phone: req.body.phone,
                    name :{
                        firstname: req.body.firstname,
                        lastname: req.body.lastname
                    },
                    contact:{
                        address: {
                            street: req.body.street,
                            houseNumber: req.body.houseNumer,
                            houseNumberAddition: req.body.houseNumberAddition,
                            zipcode: req.body.zipcode,
                            city: req.body.city,
                            country: req.body.country[1]
                        }
                    }
                }
            }),
        password, function(err, user) {
            if (err) {
                console.log(err);
                return res.redirect('/');
                throw err;
            }
            //TODO: set correct receiver
            //mailOptions.to = req.body.email;
            mailOptions.subject = "Welkom bij Oreana";
            mailOptions.text = "Er is een account voor u aangemaakt. U kunt inloggen op http://localhost:3000 \n" +
                "Inloggegevens zijn: \n" +
                req.body.email + "\n" +
                password + "\n" +
                "\n" + req.body.toString;

            console.log(mailOptions);
// send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            });
            res.redirect('back');
        }
    );



});

router.get('/countWeek/:entity', function (req, res, next) {
    console.log(req.params.entity);
    var weekAgo = new Date();
    weekAgo.setDate(weekAgo.getDate() - 7);
    User.find({
        "entity": req.params.entity,
        "profile.role": "Client",
        "created": {"$gte": weekAgo, "$lt": new Date()}
    }, function (err, users) {
        res.json({count: users.length});
    });
});

router.get('/enable/:id/:bool', function (req, res, next) {
    var enabled = req.params.bool.toLowerCase() == 'true';
    User.findByIdAndUpdate(req.params.id, {enabled: enabled}, function (err, user) {
        if (err) throw err;
        res.json({
            responseMessage: 'success',
            responseCode: 1
        });
    });
});

router.get('/getuser/:id', function(req, res, next){
   User.findById(req.params.id, function(err, user){
       if (err) throw err;
       res.json(user);
   });
});

router.get('/activeemployees/:entity', function (req, res, next) {
    User.find({"enabled": true, "profile.role": {$ne: 'client'}, "entity": req.params.entity}, function (err, users) {
        if (err) throw err;
        res.json(users);
    });
});

router.post('/update/:id', function (req, res, next) {
    var editedUser = {
        username: req.body.email,
        email: req.body.email,
        profile: {
            role: "",
            dateOfBirth: req.body.dateOfBirth,
            name: {
                firstname: req.body.firstname,
                lastname: req.body.lastname
            },
            photo: req.body.currentPhoto,
            contact: {
                address: {
                    street: req.body.street,
                    houseNumber: req.body.houseNumber,
                    houseNumberAddition: req.body.houseNumberAddition,
                    zipcode: req.body.zipcode,
                    city: req.body.city,
                    country: req.body.country[1]
                },
                phone: req.body.phone.toString()
            }
        }
    };
    if (req.body.photo != 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAFIklEQVR4Xu3VsRHAMAzEsHj/pTOBXbB9pFchyLycz0eAwFXgsCFA4C4gEK+DwENAIJ4HAYF4AwSagD9IczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhH4AStUAMmSuOW2AAAAAElFTkSuQmCC') {
        editedUser.profile.photo = req.body.photo;
    }
    User.findById(req.params.id, '+hash +salt', function (err, user) {
        if (err) {
            res.redirect('/');
            throw err;
        }
        editedUser.profile.role = user.profile.role;

        var correctPassword = false;
        //Check if correct password is entered before saving any changes
        crypto.pbkdf2(req.body.password, user.salt, 25000, 512, 'sha256', function (err, key) {
            if (err) throw err;
            correctPassword = key.toString('hex') === user.hash;
            if (correctPassword) {
                if (req.body.newPassword && req.body.confirmPassword && (req.body.newPassword.toString() === req.body.confirmPassword.toString())) {
                    user.setPassword(req.body.newPassword, function (passwordError) {
                        if (passwordError) throw passwordError;
                        user.save(function (error) {
                            if (error) {
                                res.redirect('/#/profile/failed');
                                throw error;
                            }
                            User.findByIdAndUpdate(user.id, editedUser, function (err, updatedUser) {
                                res.redirect('/#/profile/success');
                            });
                        });
                    });
                } else {
                    User.findByIdAndUpdate(req.params.id, editedUser, function (err, updatedUser) {
                        res.redirect('/#/profile/success');
                    });
                }
            } else {
                res.redirect('/#/profile/failed');
            }
        });
    });
});

module.exports = router;
