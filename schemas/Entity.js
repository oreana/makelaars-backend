var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var entitySchema = new Schema({
    created: {type: Date, default: Date.now},
    enabled: {type: Boolean, default:true},
    name: {type:String, required:true},
    logo: {type:String},
    contact: {
        address: {
            street: {type:String},
            number: {type:Number},
            numberAddition: {type:String},
            zipcode: {type:String},
            city: {type:String},
            country: {type:String}
        },
        email: {type:String},
        phone: {type:String}
    },
    product: {type:String}
});

module.exports = mongoose.model('Entity', entitySchema);