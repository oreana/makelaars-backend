var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var objectSchema = new Schema({
    created: Date,
    enabled: Boolean,
    entity: String,
    photos: Array,
    videos: Array,
    blueprint: Array,
    media: Array,
    seller: String,
    client: String,
    offeredSince: Date,
    visits: Number,
    plannedVisits: Number,
    description: Array, //Misschien meer toevoege= Advertentie, Indeling, Verdiepingen, etc...
    category: String, //Woonhuis, Appartement, Parkeergelegenheid, Bouwgrond, Overige
    kind: String, //Eensgezinswoning, Herenhuis, Villa, Landhuis, Bungalow, Woonboerderij, Grachtenpand, Woonboot, Stacaravan
    type: String, //Vrijstaand, Geschakeld, Twee onder een kap, Tussenwoning, Hoekwoning, Eindwoning, Half vrijstaand, Geschakeld twee onder een kap, Verspringend
    rent: {
        deposit: Number,
        serviceCosts: Number,
        gas: Number,
        water: Number,
        light: Number,
        vacancyLaw: Boolean
    },
    published: Boolean,
    liveDate: Date,
    acceptance: String, //In overleg, Direct
    contributionWE: Number, //Onderhoudskosten
    energyLabel: {
        class: String, //A++, A+, A, -> G
        index: Number, //belgie
        certificate: {
            present: Boolean,
            endDate: Date,
            number: Number
        }
    },
    address: {
        street: String,
        number: Number,
        numberAddition: String,
        zipcode: String,
        city: String,
        town: String,
        province: String,
        country: String
    },
    heating:{
        fuel: String, //gas, olie, elektrisch
        type: String,
        buildYear: Date,
        owner: Boolean
    },
    features: {
        price: Array,
        priceCode: String,
        priceType: String, //K.K., V.O.N, per maand
        btw: Boolean,
        rooms: Number,
        bedrooms: Number,
        garageCount: Number,
        livingArea: Number,
        volume: Number, //Inhoud van een pand
        parcel: Number,
        floorCount: Number,
        gardenCount: Number,
        constructionDate: String,
        newBuilding: Boolean,
        permantent: Boolan, //Permanente bewoning of recreatie
        shed: String, //aangebouwd steen, aangebouwd hout, vrijstaand steen, crijstaand hout, inpanding, box
        isolation: Array, //Dakisolatie, Vloerisolatie, muurisolate, volledig geisoleerd, ankerloze spouwmuuer, spouwmuur, ecobouw
        extras: Array, //tagging support
        open: {
            isOpen: Boolean,
            startDate: Date,
            endDate: Date
        },
        location: Array //tagging support
    }
});

module.exports = mongoose.model('Object', objectSchema);