var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    enabled: {type: Boolean, default:true},
    entity: {type: String},
    username: {type: String, required:true, unique:true},
    email: { type: String, required: true, unique: true },
    profile:{
        role: { type: String},
        dateOfBirth: {type: Date},
        photo: {type: String},
        name: {
            firstname: { type: String, required: true },
            lastname: { type: String, required: true }
        },
        contact: {
            address: {
                street: {type: String},
                houseNumber: {type: Number},
                houseNumberAddition: {type: String},
                zipcode: {type: String},
                city: {type: String},
                country: {type: String}
            },
            phone: {type: String}
        },
        preferences: {type: Array},
        visits:{
            object: {type: Array}
        }
    }
});

userSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', userSchema);