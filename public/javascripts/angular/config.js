oreanaApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl: 'views/oreanaVandaag.html',
            controller: 'oreanaVandaagController'
        })
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller: 'profileController'
        })
        .when('/profile/:updateStatus', {
            templateUrl: 'views/profile.html',
            controller: 'profileController'
        })
        .when('/relations/employees', {
            templateUrl: 'views/employees.html',
            controller: 'employeesController'
        })
        .when('/relations/newemployee', {
            templateUrl: 'views/newEmployee.html',
            controller: 'employeesController'
        })
        .when('/relations/editemployee/:id', {
            templateUrl: 'views/editEmployee.html',
            controller: 'employeesController'
        })
        .when('/venues', {
            templateUrl: 'views/venues.html',
            controller: 'venuesController'
        })
        .when('/contacts', {
            templateUrl: 'views/contacts.html',
            controller: 'contactsController'
        })
        .when('/calendar', {
            templateUrl: 'views/calendar.html',
            controller: 'calendarController'
        })


}]);