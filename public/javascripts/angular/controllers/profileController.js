oreanaApp.controller('profileController', ['$scope', '$rootScope', '$routeParams', function ($scope, $rootScope, $routeParams) {
    $('[data-toggle="tooltip"]').tooltip();
    $scope.tempUser = angular.copy($rootScope.user);
    $scope.photo = angular.copy($rootScope.user.profile.photo);
    $scope.actionUrl = "/api/v1/users/update/" + $rootScope.user._id;
    $scope.countries = countries;
    console.log($scope.countries);

    if ($routeParams.updateStatus){

    }


    var handleFileSelect=function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope){
                $scope.photoInput=evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    $('#photoInput').on('change',handleFileSelect);

    $scope.clearPhoto = function(){
        $scope.photoInput = '';
        $scope.tempUser.profile.photo = '';
        $('#photoInput').val('');
    }

}]);