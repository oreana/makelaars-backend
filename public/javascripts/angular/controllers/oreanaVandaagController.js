oreanaApp.controller('oreanaVandaagController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
    $('[data-toggle="tooltip"]').tooltip();

    $http.get("/api/v1/users/count/" + $rootScope.entity.name).
        then(function (response) {
            $scope.userCount = response.data.count;
        }, function (response) {
            console.log(response);
        });

    $http.get("api/v1/users/countWeek/" + $rootScope.entity.name).
        then(function (response) {
            $scope.newUserCount = response.data.count;
        }, function (response) {
            console.log(response);
        });

}]);