oreanaApp.controller('employeesController', ['$scope', '$rootScope', '$routeParams', '$http', '$location', function ($scope, $rootScope, $routeParams, $http, $location) {
    $('[data-toggle="tooltip"]').tooltip();
    $scope.countries = countries;

    $scope.activeEmployees = function(){
        $http.get("api/v1/users/activeemployees/"+entity.name).
            then(function (response) {
                console.log(response.data);
                $scope.employees = response.data;
            }, function (response) {
                console.log(response);
            });
    };

    //function initMap() {
    //    console.log('Google maps api initialized.');
    //    angular.bootstrap(document.getElementById('map'), ['oreanaApp']);
    //}
    //
    //$scope.mapOptions = {
    //    center: new google.maps.LatLng(35.784, -78.670),
    //    zoom: 15,
    //    mapTypeId: google.maps.MapTypeId.ROADMAP
    //};

    var url = $location.path();
    console.log(url);
    if (url === '/relations/employees'){
        $scope.activeEmployees();
    }

    if (url.indexOf('editemployee') > -1 && $routeParams.id) {
        var userId = $routeParams.id;
        $http.get("api/v1/users/getuser/"+userId).
            then(function (response) {
                console.log(response.data);
                $scope.employee = response.data;
                $scope.tempEmployee = angular.copy($scope.employee);
                console.log($scope.tempEmployee);
            }, function (response) {
                console.log(response);
            });
    }
}]);